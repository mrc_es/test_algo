
class BaseA():

    def atributo1(self):
        print("atributo Base A")

class BaseB(BaseA):

    def atributo1(self):
        print("atributo Base B")

class BaseC(BaseA):

    def atributo1(self):
        print("atributo Base C")

class BaseD(BaseA):

    def atributo1(self):
        print("atributo Base D")

class Prueba1(BaseB, BaseC, BaseD):

    pass


if __name__ == '__main__':
    
    objeto1 = Prueba1()

    objeto1.atributo1()