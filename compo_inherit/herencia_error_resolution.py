

class BaseB():

    def atributo1(self):
        print("atributo Base B")

class BaseA(BaseB):

    def atributo1(self):
        print("atributo Base A")

class BaseC():

    def atributo1(self):
        print("atributo Base C")

class BaseD():

    def atributo1(self):
        print("atributo Base D")

class Prueba1(BaseB, BaseA, BaseC, BaseD):

    pass


if __name__ == '__main__':
    
    objeto1 = Prueba1()

    objeto1.atributo1()