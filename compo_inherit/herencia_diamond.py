
class BaseBase():

    def atributo1(self):
        print("atributo base base")

class BaseA(BaseBase):

    def atributo1(self):
        print("atributo Base A")

class BaseB(BaseBase):
    pass

class Prueba1(BaseA, BaseB):

    pass


if __name__ == '__main__':
    
    objeto1 = Prueba1()

    objeto1.atributo1()