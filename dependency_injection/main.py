"""Dependency injection example, Cars & Engines."""

import cars
import engines


if __name__ == '__main__':
    gasoline_car = cars.Car(engines.GasolineEngine())
    diesel_car = cars.Car(engines.DieselEngine())
    electric_car = cars.Car(engines.ElectricEngine())

    gasoline_car._engine.start()

