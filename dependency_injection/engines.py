"""Dependency injection example, engines module."""

class Engine:
    """Example engine base class.

    Engine is a heart of every car. Engine is a very common term and
    could be implemented in very different ways.
    """
    pass

class GasolineEngine(Engine):
    """Gasoline engine."""
    def start(self):
        print("hola")

class DieselEngine(Engine):
    """Diesel engine."""
    pass

class ElectricEngine(Engine):
    """Electric engine."""
    pass