"""Dependency injection  Cars & Engines IoC containers."""

import cars
import engines

import dependency_injector.containers as containers
import dependency_injector.providers as providers


class Engines(containers.DeclarativeContainer):
    """IoC container of engine providers."""

    gasoline = providers.Factory(engines.GasolineEngine)

    diesel = providers.Factory(engines.DieselEngine)

    electric = providers.Factory(engines.ElectricEngine)


class Cars(containers.DeclarativeContainer):
    """IoC container of car providers."""

    gasoline = providers.Factory(cars.Car,
                                 engine=Engines.gasoline)

    diesel = providers.Factory(cars.Car,
                               engine=Engines.diesel)

    electric = providers.Factory(cars.Car,
                                 engine=Engines.electric)


if __name__ == '__main__':
    gasoline_car = Cars.gasoline()
    diesel_car = Cars.diesel()
    electric_car = Cars.electric()

    gasoline_car._engine.start()