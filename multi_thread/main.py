from time import sleep
import threading

class FakeDB:

    def __init__(self):
        pass

    def get(self):
        print("obtuvo")
    
    def insert(self):
        print("inserto")

    def delete(self):
        print("borro")

def timer(db='', continuar=True):

    i = 1

    while i < 13:
        print(f'Tiempo: {i}')

        if i == 10:
            deleter(db, False)
        if i == 12:
            inserter(db, False)

        i = i + 1
        sleep(1)
        
    print("timer termino")

def deleter(db, continuar):

    while continuar:
        db.delete()
        sleep(1)
    
    print("deleter paro")

def inserter(db, continuar):

    while continuar:
        db.insert()
        sleep(2)

    print("inserter paro")

def main():

    db = FakeDB()

    t1 = threading.Thread(target=timer, kwargs={'db':db, 'continuar':True})
    t2 = threading.Thread(target=inserter, args=[db, True])
    t3 = threading.Thread(target=deleter, args=[db, True])

    t1.start()
    t2.start()
    t3.start()


if __name__ == '__main__':
    main()