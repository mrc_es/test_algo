from flask import Flask
from flask import jsonify

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello, World!"

@app.route("/ruta2")
def hello2():
    return "Hello, World!2"

@app.route("/ruta2/<parametro1>")
def ruta3(parametro1):
    return f"Hello, {parametro1}"

@app.route("/ruta3/<parametro1>")
def ruta_json(parametro1):

    return jsonify(parametro1="parametro1", response=parametro1), 201